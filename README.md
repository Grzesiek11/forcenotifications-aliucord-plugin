# ForceNotifications Aliucord Plugin

A plugin for [Aliucord](https://github.com/Aliucord/Aliucord), which forces
Android notifications when the client is open

Commissioned by [@anhJer](https://discord.com/users/241356312741412865)

## Download

- [Latest plugin ZIP](https://github.com/jedenastka/Aliucord-Plugin-Index/raw/builds/ForceNotifications.zip)
- [#plugins-list](https://discord.com/channels/811255666990907402/811275162715553823/1208205954949062778)
